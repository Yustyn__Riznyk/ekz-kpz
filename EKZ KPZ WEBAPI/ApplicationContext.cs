﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EKZ_KPZ.Models;

namespace EKZ_KPZ
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Book> Book_Table { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=HOME-GAME-PC1\SQLEXPRESS;Database=tempDataBase;Trusted_Connection=True;");
        }
    }
}
