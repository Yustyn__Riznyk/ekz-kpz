﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EKZ_KPZ.Models
{
    public class Book
    {
        public int BookID { get; set; }
        public string Name { get; set; }
        public string AuthorName { get; set; }
        public int NumberOfBooks { get; set; }
    }
}
