﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EKZ_KPZ.Models;

namespace EKZ_KPZ.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class BookController : ControllerBase
    {
        [HttpGet]
        public ActionResult GetBook()
        {
            IEnumerable<Book> books = null;
            using (var context = new ApplicationContext())
            {
                books = context.Book_Table.ToList();
            }
            if (books.Count() == 0)
            {
                return NotFound();
            }
            return Ok(books);
        }
        [HttpPost]
        public ActionResult PostBook(Book book)
        {
            using (var context = new ApplicationContext())
            {
                context.Book_Table.Add(new Book
                {
                    Name = book.Name,
                    AuthorName = book.AuthorName,
                    NumberOfBooks = book.NumberOfBooks
                });
                context.SaveChanges();
            }
            return Ok();     
        }
        [HttpPut("{id}")]
        public ActionResult PutBook(int id, Book book)
        {
            using (var context = new ApplicationContext())
            {
                var existingBook = context.Book_Table.Where(b => b.BookID == id).FirstOrDefault();
                if (existingBook != null)
                {
                    existingBook.Name = book.Name;
                    existingBook.AuthorName = book.AuthorName;
                    existingBook.NumberOfBooks = book.NumberOfBooks;
                    context.SaveChanges();
                }
                else
                    return NotFound();
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult DeleteBook(int id)
        {
            using (var context = new ApplicationContext())
            {
                var book = context.Book_Table.Where(b => b.BookID == id).FirstOrDefault();
                context.Book_Table.Remove(book);
                context.SaveChanges();
            }
            return Ok();
        }
    }
}
